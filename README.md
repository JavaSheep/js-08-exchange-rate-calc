Aufgaben Währungsrechner
========================

Die Datei `lib.js` behinhaltet einen fiktiven Service der Wechselkurse ausliefert.

Die Funktion `getSupportedCurrencies()` liefert einen Array der verfügbaren Währungen.

Die Funktion `getExchangeRate(fromCurrency, toCurrency)` liefert den Wechselkurs für die gebenden Währungen.

Aufgaben 1
----------

Erstelle eine Webseite die einige Wechselkurse ausgibt.

Benutze hierfür Konstrukte aus der imperativen Programmierung.

Aufgabe 2
----------

Erstelle eine Webseite die drei Eingabefelder bereitstellt:
   * Ausgangswährung
   * Zielwährung
   * Betrag
   
Erstelle eine Lösung die den Betrag in der Zielwährung bereitstellt, nachdem ein Button ausgewählt wurde.

Erstelle eine Lösung die den Betrag immer dann ausgibt wenn alle drei Felder belegt sind und passe die Ausgabe bei Änderungen des Feldinhalts an.

Nutze für die Berechnung weiterhin imperative Konstrukte, beachte jedoch das die Nutzerinteraktion asynchron funktioniert.

Aufgabe 3
----------

Erweitere die Bibliothek so, das sie objektorientiert arbeitet.

Aufgabe 4
---------

Erweitere die Bibliothek so, das der Abruf der Wechselkurse asynchron funktioniert. Erweitere die Webseite so, das zu einer
Währung der Betrag immer in alle anderen Währungen umgerechnet wird.